package com.metrohospital.app.retrofit;

public class DataObject {

    public SectionA sectionA = new SectionA();
    public SectionB sectionB = new SectionB();


    public class SectionA {
        public Boolean isFollowUpSample = null;
        public PatientDetails patientDetails = new PatientDetails();
        public PatientCategory patientCategory = new PatientCategory();

        public class PatientDetails {
            public String patientName = null;
            public Boolean patientInQuarantineFacility = null;
            public String presentVillageOrTown = null;
            public String districtOfPresentResident = null;
            public String stateOfPresentResident = null;
            public String presentPatientAddress = null;
            public String pinCode = null;

            public String age = null;
            public boolean isAgeInYear = true;
            public Gender gender = Gender.NONE;
            public String mobileNumber = null;
            public MobileBelongsTo mobileNumberBelongsToSelf = MobileBelongsTo.NONE;
            public String nationality = null;
            public Boolean downloadedAarogyaSetuApp = null;
        }

        public class PatientCategory {
            public boolean cat1 = false;
            public boolean cat2 = false;
            public boolean cat3 = false;
            public boolean cat4 = false;
            public boolean cat5a = false;
            public boolean cat5b = false;
            public boolean cat6 = false;
            public boolean cat7 = false;
            public boolean cat8 = false;
            public boolean cat9 = false;
            public boolean cat10 = false;
        }
    }

    public class SectionB {
        public ClinicalSymptomsAndSigns clinicalSymptomsAndSigns = new ClinicalSymptomsAndSigns();
        public PreExistingMedicalCondition preExistingMedicalCondition = new PreExistingMedicalCondition();
        public HospitalizationDetails hospitalizationDetails = new HospitalizationDetails();

        public class ClinicalSymptomsAndSigns {
            public Boolean symptoms = null;
            public boolean cough = false;
            public boolean breathlessness = false;
            public boolean soreThroat = false;
            public boolean diarrhoea = false;
            public boolean nausea = false;
            public boolean chestPain = false;
            public boolean vomiting = false;
            public boolean haemoptysis = false;
            public boolean nasalDischarge = false;
            public boolean feverAtEvaluation = false;
            public boolean bodyAche = false;
            public boolean sputum = false;
            public boolean abdominalPain = false;
        }

        public class PreExistingMedicalCondition {
            public boolean chronicLungDisease = false;
            public boolean chronicRenalDisease = false;
            public boolean immunocompromisedCondition = false;
            public boolean malignancy = false;
            public boolean diabetes = false;
            public boolean heartDisease = false;
            public boolean hyperTension = false;
            public boolean chronicCondition = false;
        }

        public class HospitalizationDetails {
            public Boolean hospitalized = null;
            public String hospitalIdOrNumber = null;
            public String hospitalizationDate = null;
            public String hospitalState = null;
            public String hospitalDistrict = null;
            public String hospitalName = null;
        }
    }


    public enum Gender {
        MALE, FEMALE, OTHERS, NONE
    }

    public enum MobileBelongsTo {
        SELF, FAMILY, NONE
    }


}
