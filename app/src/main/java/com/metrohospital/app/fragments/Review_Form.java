package com.metrohospital.app.fragments;

import androidx.fragment.app.Fragment;

public class Review_Form extends Fragment {

//    private MainViewModel mViewModel;
//    private TextView textview;
//    private ProgressDialog progress;
//    DataObject.SectionA.PatientDetails patientDetails;
//
//
//    @Override
//    public View onCreateView(
//            LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState
//    ) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_review_form, container, false);
//    }
//
//    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        mViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
//        textview = view.findViewById(R.id.textview);
//        progress=new ProgressDialog(getContext());
//
//
//        view.findViewById(R.id.actionPrev).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NavHostFragment.findNavController(Review_Form.this)
//                        .navigate(R.id.action_Review_Form_TO_Section_B_3);
//            }
//        });
//
//        view.findViewById(R.id.actionNext).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                callApi();
//              //  Snackbar.make(view, "Call api to submit the form!", Snackbar.LENGTH_LONG).show();
//                //TODO: On successful submission reset the viewmodel by calling mViewModel.resetData();
//            }
//        });
//
//        mViewModel.dataObject.observe(getViewLifecycleOwner(), new Observer<DataObject>() {
//            @Override
//            public void onChanged(DataObject dataObject) {
//                DataObject.SectionA sectionA = dataObject.sectionA;
//                DataObject.SectionB sectionB = dataObject.sectionB;
//
//                 patientDetails = sectionA.patientDetails;
//                DataObject.SectionA.PatientCategory patientCategory = sectionA.patientCategory;
//
//                DataObject.SectionB.ClinicalSymptomsAndSigns clinicalSymptomsAndSigns = sectionB.clinicalSymptomsAndSigns;
//                DataObject.SectionB.PreExistingMedicalCondition preExistingMedicalCondition = sectionB.preExistingMedicalCondition;
//                DataObject.SectionB.HospitalizationDetails hospitalizationDetails = sectionB.hospitalizationDetails;
//
//
//                StringBuilder stringBuilder = new StringBuilder();
//
//                stringBuilder.append("Test Intimation Details").append("\n");
//                stringBuilder.append("Follow up sample: ")
//                        .append(sectionA.isFollowUpSample == null ? "---"
//                                : (sectionA.isFollowUpSample ? "YES" : "NO")).append("\n\n\n");
//
//
//                stringBuilder.append("Personal Details").append("\n");
//                stringBuilder.append("Patient Name: ").append(patientDetails.patientName).append("\n");
//                stringBuilder.append("Patient In Quarantine Facility: ")
//                        .append(patientDetails.patientInQuarantineFacility == null ? "---"
//                                : (patientDetails.patientInQuarantineFacility ? "YES" : "NO")).append("\n");
//                stringBuilder.append("Present Village or Town: ").append(patientDetails.presentVillageOrTown).append("\n");
//                stringBuilder.append("District of present residence: ").append(patientDetails.districtOfPresentResident).append("\n");
//                stringBuilder.append("Present patient address: ").append(patientDetails.presentPatientAddress).append("\n");
//                stringBuilder.append("Pincode: ").append(patientDetails.pinCode).append("\n");
//                stringBuilder.append("Age in years: ").append(patientDetails.age).append("\n");
//                stringBuilder.append("Gender: ").append(patientDetails.gender).append("\n");
//                stringBuilder.append("Mobile number: ").append(patientDetails.mobileNumber).append("\n");
//                stringBuilder.append("Mobile number belongs to: ").append(patientDetails.mobileNumberBelongsToSelf).append("\n");
//                stringBuilder.append("Nationality: ").append(patientDetails.nationality).append("\n");
//                stringBuilder.append("Downloaded Aarogya Setu App: ")
//                        .append(patientDetails.downloadedAarogyaSetuApp == null ? "---"
//                                : (patientDetails.downloadedAarogyaSetuApp ? "YES" : "NO")).append("\n");
//
//                //TODO: append other data to the stringbuilder
//
//
//                textview.setText(stringBuilder.toString());
//            }
//        });
//    }
//
//    private void callApi() {
//
//        progress.show();
//        InterfaceApi api = ApiClient.getClient().create(InterfaceApi.class);
//        Call<StatusModel> call = api.savecovid(ApiClient.Apikey,
//                                             patientDetails.patientName,
//                                            patientDetails.age,
//                                            "1",
//                                            patientDetails.mobileNumber,
//                                            patientDetails.presentPatientAddress,
//                                            "1,5,4",
//                                            "yes",
//                                            "cough,diarrhoea",
//                                            "chronic lung disease,Diabetes, heart disease",
//                                            "no"
//                                             );
//        call.enqueue(new Callback<StatusModel>() {
//            @Override
//            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
//                final StatusModel status = response.body();
//
//                if (status.getStatus().equals("1")) {
//                    progress.hide();
//                   // Snackbar.make(view, "Call api to submit the form!", Snackbar.LENGTH_LONG).show();
//                     Toast.makeText(getContext(), "Data save successfully", Toast.LENGTH_SHORT).show();
//                    //  openActivity(data.getResponse().get(0).getEmp_id(),data.getResponse().get(0).getMob_no());
//
//                } else {
//                    progress.hide();
//                    Toast.makeText(getContext(), "Data save successfully", Toast.LENGTH_SHORT).show();
//                }
//            }
//            @Override
//            public void onFailure(Call<StatusModel> call, Throwable t) {
//            }
//        });
//
//    }
}