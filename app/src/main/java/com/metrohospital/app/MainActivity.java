package com.metrohospital.app;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.metrohospital.app.databinding.ActivityMainBinding;
import com.metrohospital.app.retrofit.DataObject;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private boolean isProcessingData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);


        binding.mainLayout.section5.actionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isProcessingData) {
                    DataObject dataObject = createDataObject();
                    //TODO: Transform $dataObject and call api
                }
            }
        });
    }

    private DataObject createDataObject() {
        DataObject dataObject = new DataObject();

        int folllowUpId = binding.mainLayout.section1.followUpSampleRV.getCheckedRadioButtonId();
        dataObject.sectionA.isFollowUpSample = folllowUpId == -1 ? null : (folllowUpId == R.id.yes);
        dataObject.sectionA.patientDetails.patientName = binding.mainLayout.section1.patientNameET.getText().toString();
        int isInQuarantine = binding.mainLayout.section1.patientInQuarantine.getCheckedRadioButtonId();
        dataObject.sectionA.patientDetails.patientInQuarantineFacility = isInQuarantine == -1 ? null : (isInQuarantine == R.id.quarantine_yes);
        dataObject.sectionA.patientDetails.districtOfPresentResident = binding.mainLayout.section1.districtET.getText().toString();
        dataObject.sectionA.patientDetails.stateOfPresentResident = binding.mainLayout.section1.stateET.getText().toString();
        dataObject.sectionA.patientDetails.presentPatientAddress = binding.mainLayout.section1.presentAddressET.getText().toString();
        dataObject.sectionA.patientDetails.pinCode = binding.mainLayout.section1.pinET.getText().toString();
        dataObject.sectionA.patientDetails.age = binding.mainLayout.section1.ageET.getText().toString();
        int gender = binding.mainLayout.section1.genderRB.getCheckedRadioButtonId();
        dataObject.sectionA.patientDetails.gender = gender == -1 ? DataObject.Gender.NONE : (gender == R.id.male ? DataObject.Gender.MALE : DataObject.Gender.FEMALE);
        dataObject.sectionA.patientDetails.mobileNumber = binding.mainLayout.section1.mobileET.getText().toString();
        int mobile = binding.mainLayout.section1.mobileBelongsTo.getCheckedRadioButtonId();
        dataObject.sectionA.patientDetails.mobileNumberBelongsToSelf = mobile == -1 ? DataObject.MobileBelongsTo.NONE : (mobile == R.id.self ? DataObject.MobileBelongsTo.SELF : DataObject.MobileBelongsTo.FAMILY);
        dataObject.sectionA.patientDetails.nationality = binding.mainLayout.section1.nationalityET.getText().toString();
        int aarogya = binding.mainLayout.section1.aarogyaSetuApp.getCheckedRadioButtonId();
        dataObject.sectionA.patientDetails.downloadedAarogyaSetuApp = aarogya == -1 ? null : aarogya == R.id.downloaded;
        
        //TODO: UPDATE ${dataObject.sectionA.patientCategory}
        //TODO: UPDATE ${dataObject.sectionB.clinicalSymptomsAndSigns}
        //TODO: UPDATE ${dataObject.sectionB.preExistingMedicalCondition}
        //TODO: UPDATE ${dataObject.sectionB.hospitalizationDetails}
        
        return dataObject;
    }


}